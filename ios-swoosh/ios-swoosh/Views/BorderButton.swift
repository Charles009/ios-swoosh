//
//  BorderButton.swift
//  ios-swoosh
//
//  Created by Charles Huang on 27/11/17.
//  Copyright © 2017 Charles Huang. All rights reserved.
//

import UIKit

class BorderButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 3.0
        layer.borderColor = UIColor.white.cgColor
    }
}
